.. _sync:

Synchronization
===========================

If you have Google Chrome installed on multiple devices you can synchronize
quicktexts between machines. Here's how you do it:

Please make sure that you have the `Quicktext` extension installed_ on you
Google Chrome before following the steps bellow.

 1. Open the 'Options' for `Quicktext` from Extensions menu in Google Chrome.
 2. `Login` or `Register` if your not yet registered on the https://quicktext.io/ website.
 3. Click on `Sync Now` button.

Now all your quicktexts should be synchronized with the Synchronization server.

Now in order to get your quicktexts synchronized with other machines repeat
the steps above (but this time just Login).

How does synchronization work?
---------------------------------

Every time the synchronization process starts all your quicktexts are uploaded
and stored to the https://quicktext.io/ via a secure HTTPS connection.

All your quicktexts can now be shared between computers or with other members of your team.

Syncing not only uploads all your quicktexts it also updates the quicktexts
that have been modified on other computers or downloads the new quicktexts shared
by your team.

.. note:: No Quicktexts are ever deleted! Unless you specifically demand it.
          In order to completely delete your data please send an e-mail to support@quicktext.io

.. _installed: https://chrome.google.com/webstore/detail/quicktext-for-gmail/fbkpbekdjdelappaffjlbfffidknkeko
