.. _automatic-suggestions:

Automatic Suggestions
======================

Automatic suggestions helps finding the correct quicktext as you type.
Imagine that you have a quicktext that explains to your users that they have 
to check their spam inbox::

    Quicktext Title: Add note about Spam folder
    Quicktext Body: Note: If you can't find the e-mail, please check your
                    'Spam' folder in order to find the missing e-mail.

Now when you type spam in the body of your e-mail a small popup will appear 
where you can choose the correct quicktext:

{Screenshot - preferably gif}

When the automatic suggestions are triggered?
------------------------------------------------

Automatic suggestions are triggered when you type a word that is found in your
quicktext in the following order:

 - The `title` of the quicktext contains the word.
 - Is one of the `tags` of the quicktext.
 - The `body` of the quicktext contains the word.

Disabling Automatic Suggestions
---------------------------------

Automatic Suggestions are great, but if you want to disable them for some 
reason you can do so by going to:

 1. Google Chrome
 2. Customize and Control Google Chrome (look for this icon: {TODO})
 3. Tools
 4. Extensions
 5. Find `Quicktext` extension in the list
 6. Options
 7. Uncheck `Automatic Suggestions`

You can also get to the Options faster by right-clicking the `Quicktext` icon
in the top right corner (inside the Navigation bar. Next to the 'Bookmarks' star icon) 
and selecting Options.

.. note:: The `Quicktext` icon only appears when using Gmail.
