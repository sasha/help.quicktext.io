.. _tags:

Tags
========

Tags help with filtering and categorization of quicktexts. You can add a tag to
a quicktext by updating the 'Tags' field.

Once that's done you can start filtering your quicktexts by clicking the tags 
you want to filter by.

.. note:: Tags get get highlighed when you click on then show which tags you are filtering by.

You can you can also use :ref:`search` if you want to filter by other criteria.
