.. _tab-completion:

Tab Completion
===============

Tab completion inserts quicktexts by typing a `tab completion` keyword and then 
hitting the Tab key. You can see and example of tab completion usage in the
following video:

[video]

Disabling Tab completion
---------------------------------

Tab completion is cool, but if you want to disable it for some reason you can do so by going to:

 1. Google Chrome
 2. Customize and Control Google Chrome
 3. Tools
 4. Extensions
 5. Quicktext for Gmail
 6. Options
 7. Uncheck `Tab Completion` option.

You can also get to the Options faster by right-clicking the Quicktext extension
icon in the top right corner and selecting Options.

.. note:: The `Quicktext` icon only appears when using Gmail.
