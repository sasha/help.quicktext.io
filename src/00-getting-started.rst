.. _getting-started:

Getting Started
================

`Quicktext` is a Google Chrome extension that allows Gmail users to write
e-mails faster by using :ref:`automatic-suggestions`, :ref:`tab-completion`,
:ref:`templates` and other features.

.. raw:: html

    <object width="480" height="385"><param name="movie"
    value="http://www.youtube.com/v/G1WZDdT9eNU&hl=en_US&fs=1&rel=0"></param><param
    name="allowFullScreen" value="true"></param><param
    name="allowscriptaccess" value="always"></param><embed
    src="http://www.youtube.com/v/G1WZDdT9eNU&hl=en_US&fs=1&rel=0"
    type="application/x-shockwave-flash" allowscriptaccess="always"
    allowfullscreen="true" width="480"
    height="385"></embed></object>

Install_ the extension!

`Quicktext` can also be used in tandem with the https://quicktext.io/ website to
enable features such as Team Sharing, Backup, Usage Statistics and other features.

Use cases
---------

So far there are 2 major use cases for `Quicktext`. Feel free to choose the one
that fits your use case the best.

Standalone Quicktext Google Chrome extension
+++++++++++++++++++++++++++++++++++++++++++++

If you use Gmail on your own (you don't have a team) then you don't have to
login or register on the https://quicktext.io/ website. You can simply install
the extension here and use it on your own.

However, if you like store your quicktexts online and synchronize them between
devices you can create an account here.


Using Quicktext in a team
+++++++++++++++++++++++++

If you work in a team and you want to share and collaborate on your quicktexts
with your team members then you're in luck! Because https://quicktext.io allows
doing just that.

All you have to do is create an account on this website. Add some users and
start working together. More on this topic in the Sharing section.

.. _Install: https://chrome.google.com/webstore/detail/quicktext-for-gmail/fbkpbekdjdelappaffjlbfffidknkeko
