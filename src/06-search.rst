.. _search:

Search
=======

Once you and your team start to have a lot of quicktexts finding the right one
can be complicated. Here's where searching comes in handy.

You can search by keywords in the `title` or  `body` of your `quicktext`.
You can also filter quicktexts by using :ref:`tags`.
