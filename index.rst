Welcome to Quicktext's documentation!
=============================================

`Quicktext` is Google Chrome extension that allows Gmail users to write
e-mails faster by using :ref:`automatic-suggestions`, :ref:`tab-completion`,
:ref:`templates` and other features.

.. raw:: html

    <object width="480" height="385"><param name="movie"
    value="http://www.youtube.com/v/G1WZDdT9eNU&hl=en_US&fs=1&rel=0"></param><param
    name="allowFullScreen" value="true"></param><param
    name="allowscriptaccess" value="always"></param><embed
    src="http://www.youtube.com/v/G1WZDdT9eNU&hl=en_US&fs=1&rel=0"
    type="application/x-shockwave-flash" allowscriptaccess="always"
    allowfullscreen="true" width="480"
    height="385"></embed></object>

Install_ the extension!

Find out more at: https://quicktext.io/

.. include README.rst

.. toctree::
   :maxdepth: 2

   src/00-getting-started
   src/01-automatic-suggestions
   src/02-tab-completion
   src/03-sync
   src/04-templates
   src/05-tags
   src/06-search


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _Install: https://chrome.google.com/webstore/detail/quicktext-for-gmail/fbkpbekdjdelappaffjlbfffidknkeko
